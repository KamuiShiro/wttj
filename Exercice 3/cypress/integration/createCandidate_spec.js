import CandidateCreateForm from './pages/CandidateCreateForm';

describe('CandidateCreation', function() {
  beforeEach(function() {
    // runs before each test in the block
    cy.log("Log into the application")
  	cy.setCookie("remember_me", "eyJfcmFpbHMiOnsibWVzc2FnZSI6Ilcxc3pPRFV3TkYwc0lpUXlZU1F4TUNSemREa3lWVU5sTTFoRmJVeFVZbkI1UVM1WExsTjFJaXdpTVRVM09EVXhNVEk1TkM0NU9ESXpNekEySWwwPSIsImV4cCI6IjIwMjAtMDEtMjJUMTk6MjE6MzQuOTgyWiIsInB1ciI6bnVsbH19--29d0768a894052037626a1088a64e9de61565cc5")
  	cy.visit('https://www.welcomekit.co/dashboard/o/dkxzma3/jobs/2XMOI_yq66e6b/new-candidate/392777')
  })

  it('Create a candidate with just the required data', function() {
  	const timestamp = Date.now()
  	const form = new CandidateCreateForm();
  	form.fillFirstName("John")
  	form.fillLastName("Doe " + timestamp)
  	form.fillEmail("john.doe@test.fr")
  	form.save()
  	cy.log("Check that a card was created for this candidate in the pipeline")
  	cy.contains( "John Doe " + timestamp).should('be.visible')
  })

  it('Create a candidate with optional data', function() {
  	const timestamp = Date.now()
  	const form = new CandidateCreateForm();
  	form.fillFirstName("John")
  	form.fillLastName("Doe " + timestamp)
  	form.fillEmail("john.doe@test.fr")
  	form.fillSubtitle("QA Manager")
  	form.fillPhone("+336123456789")
  	form.fillAddress("8 boulevard Heyrault")
  	form.fillZipCode("75006")
  	form.fillCity("Paris")
  	form.save()
  	cy.log("Check that a card was created for this candidate in the pipeline")
  	cy.contains( "John Doe " + timestamp).should('be.visible')
  })

  it('Try to create a candidate with missing mandatory data', function() {
  	const timestamp = Date.now()
  	const form = new CandidateCreateForm();
  	form.fillLastName("Doe " + timestamp)
  	form.fillEmail("john.doe@test.fr")
  	cy.log("Check that save option is not available")
  	form.getSaveButton().should('not.exist')
  })

  it('Try to create a candidate with bad written email', function() {
  	const timestamp = Date.now()
  	const form = new CandidateCreateForm();
  	form.fillEmail("john.doe")
  	form.fillFirstName("John")
  	form.fillLastName("Doe " + timestamp)
  	cy.log("Check that alert icon is displayed near the email field")
  	form.getEmailLabelAlert().should('be.visible')
  	cy.log("Check that save option is not available")
  	form.getSaveButton().should('not.exist')
  })


})
