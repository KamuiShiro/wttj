class CandidateCreateForm {
  getFirstName() {
    return cy.get('[name=firstname]');
  }
  
  getLastName() {
    return cy.get('[name=lastname]');
  }
  
  getEmail() {
    return cy.get('[name=email]');
  }
  
  getSubtitle() {
    return cy.get('[name=subtitle]');
  }
  
  getPhone() {
    return cy.get('[name=phone]');
  }
  
  getAddress() {
    return cy.get('[name=address]');
  }
  
  getCity() {
    return cy.get('[name=city]');
  }
  
  getZipCode() {
    return cy.get('[name=zip_code]');
  }

  getSaveButton() {
    return cy.get('.form-actions-sticky button');
  }

  getEmailLabelAlert() {
    return cy.get('div.email label i.zmdi-alert-circle');
  }

  fillFirstName(value) {
    cy.log("Fill in first name")
    const field = this.getFirstName()
    field.clear();
    field.type(value);
    return this;
  }
  
  fillLastName(value) {
    cy.log("Fill in last name")
    const field = this.getLastName()
    field.clear();
    field.type(value);
    return this;
  }
  
  fillSubtitle(value) {
    cy.log("Fill in subtitle")
    const field = this.getSubtitle()
    field.clear();
    field.type(value);
    
    return this;  
  }
  
  fillEmail(value) {
    cy.log("Fill in email")
    const field = this.getEmail()
    field.clear();
    field.type(value);
    
    return this;
  }
  
  fillPhone(value) {
    cy.log("Fill in phone")
    const field = this.getPhone()
    field.clear();
    field.type(value);
    
    return this;
  }
  
  fillAddress(value) {
    cy.log("Fill in address")
    const field = this.getAddress()
    field.clear();
    field.type(value);
    
    return this;
  }
  
  fillCity(value) {
    cy.log("Fill in city")
    const field = this.getCity()
    field.clear();
    field.type(value);
    
    return this;
  }
  
  fillZipCode(value) {
    cy.log("Fill in zip code")
    const field = this.getZipCode()
    field.clear();
    field.type(value);
    
    return this;
  }
  
  save() {
    cy.log("Click on Save button")
    const button = this.getSaveButton()
    button.click();
  }
}

export default CandidateCreateForm;