import csv
import os
import unittest
from unittest.mock import MagicMock


class JobOffers:
    jobProfessions = []
    jobOpportunities = []
    jobCategories = ["NONE"]  # "NONE" will be used anytime a profession doesn't have a known category

    def __init__(self, **kwargs):
        """Constructor

        :param kwargs: if defined with "professions_file" and "opportunities_file" parameters, create an instance of
        JobOffers with data from those files by using the path provided
        If there is no argument, create an instance of JobOffers without loading any data
        """
        if kwargs.__len__() > 0:
            professions_file = kwargs.get("professions_file")
            opportunities_file = kwargs.get("opportunities_file")
            with open(professions_file, encoding="utf-8") as csvFile:
                csv_reader = csv.reader(csvFile, delimiter=",")
                next(csv_reader, None)  # skip the headers
                self.loadJobProfessions(csv_reader)
            with open(opportunities_file, encoding="utf-8") as csvFile:
                csv_reader = csv.reader(csvFile, delimiter=",")
                next(csv_reader, None)  # skip the headers
                self.loadJobOpportunities(csv_reader)

    # TODO: add try / catch to handle csv rows with not enough fields - not mandatory for MVP version
    # TODO: add a check to make sure profession_id is an int and is unique - not mandatory for MVP version
    def loadJobProfessions(self, csv_reader):
        """Update the current instance jobProfessions attribute with data provided

        :param csv_reader: data read from a csv file
        :return: Void
        Empty fields are replaced by the "UNDEFINED" value
        Leading and trailing spaces are removed, and text is converted to uppercase
        """
        for profession_id, profession_name, category_name in csv_reader:
            category = "UNDEFINED" if category_name == "" else category_name.strip().upper()
            job = {"profession_id": "UNDEFINED" if profession_id == "" else profession_id.strip(),
                   "profession_name": "UNDEFINED" if profession_name == "" else profession_name.strip().upper(),
                   "category_name": category}
            self.jobProfessions.append(job)
            if category not in self.jobCategories:
                self.jobCategories.append(category)

    # TODO: add try / catch to handle csv rows with not enough fields - not mandatory for MVP version
    # TODO: add a check to make sure profession_id is an int - not mandatory for MVP version
    def loadJobOpportunities(self, csv_reader):
        """Update the current instance jobOpportunities attribute with data provided

        :param csv_reader: data read from a csv file
        :return: Void
        Empty fields are replaced by the "UNDEFINED" value
        Leading and trailing spaces are removed, and text is converted to uppercase
        """
        for profession_id, contract_type, name, office_latitude, office_longitude in csv_reader:
            job_opportunity = {"profession_id": "UNDEFINED" if profession_id == "" else profession_id.strip(),
                               "contract_type": "UNDEFINED" if contract_type == "" else contract_type.strip().upper(),
                               "name": "UNDEFINED" if name == "" else name.strip().upper(),
                               "office_latitude": "UNDEFINED" if office_latitude == "" else office_latitude,
                               "office_longitude": "UNDEFINED" if office_longitude == "" else office_longitude}
            self.jobOpportunities.append(job_opportunity)

    def getJobCategory(self, profession_id):
        """Get the job category name from the profession id

        :param profession_id: id of the profession whose category we want to get
        :return: job category of the target profession if known, else "NONE"
        """
        for jobCategory in self.jobProfessions:
            if jobCategory["profession_id"] == profession_id:
                return jobCategory["category_name"]
        return "NONE"
    
    def groupByContractAndCategory(self):
        """Get the number of job offers for each professional category and contract_type

        :return: dictionary with the number of job opportunities per category per contract_type
        """
        stats = {"TOTAL": {"TOTAL": 0}}  # initialize the dictionary that will be returned
        for cat in self.jobCategories:
            stats["TOTAL"][cat] = 0
        for job in self.jobOpportunities:  # loop over all the job offers and add 1 to the relevant counters
            category = self.getJobCategory(job["profession_id"])
            contract_type = job["contract_type"]
            stats["TOTAL"][category] += 1
            if contract_type not in stats:
                stats[contract_type] = {"TOTAL": 0}
                for cat in self.jobCategories:
                    stats[contract_type][cat] = 0
            stats[contract_type][category] += 1
            stats[contract_type]["TOTAL"] += 1
            stats["TOTAL"]["TOTAL"] += 1
        return stats


class JobOffersTests(unittest.TestCase):

    # 100% line coverage, 100% decision coverage
    def test_loadJobProfessions_nominalCase(self):
        mock_csv_reader = MagicMock()
        mock_csv_reader.__iter__.return_value = iter([["1", "teSt ", " TeCH"], ["2", " dev ", " TECH "], ["", "", ""]])
        job_offers = JobOffers()
        job_offers.loadJobProfessions(mock_csv_reader)
        self.assertEqual([{"profession_id": "1", "profession_name": "TEST", "category_name": "TECH"},
                          {"profession_id": "2", "profession_name": "DEV", "category_name": "TECH"},
                          {"profession_id": "UNDEFINED", "profession_name": "UNDEFINED", "category_name": "UNDEFINED"}],
                         job_offers.jobProfessions)
        self.assertEqual(["NONE", "TECH", "UNDEFINED"], job_offers.jobCategories)

    # 100% line coverage, 100% decision coverage
    def test_loadJobOpportunities_nominalCase(self):
        mock_csv_reader = MagicMock()
        mock_csv_reader.__iter__.return_value = iter(
            [["", "", "", "", ""], ["2", " INTERNship", " qa lead ", "48.885247", "2.3566441"]])
        job_offers = JobOffers()
        job_offers.loadJobOpportunities(mock_csv_reader)
        self.assertEqual([{"profession_id": "UNDEFINED", "contract_type": "UNDEFINED", "name": "UNDEFINED",
                           "office_latitude": "UNDEFINED", "office_longitude": "UNDEFINED"},
                          {"profession_id": "2", "contract_type": "INTERNSHIP", "name": "QA LEAD",
                           "office_latitude": "48.885247", "office_longitude": "2.3566441"}],
                         JobOffers.jobOpportunities)

    # 100% line coverage, 100% decision coverage
    def test_getJobCategory_nominalCase(self):
        job_offers = JobOffers()
        job_offers.jobProfessions = [{"profession_id": "12", "profession_name": "TEST", "category_name": "TECH"},
                                     {"profession_id": "UNDEFINED", "profession_name": "UNDEFINED", "category_name": "UNDEFINED"}]
        self.assertEqual("TECH", job_offers.getJobCategory("12"))
        self.assertEqual("NONE", job_offers.getJobCategory("13"))

    def test_groupByContractAndCategory(self):
        job_offers = JobOffers()
        job_offers.jobCategories = ["NONE", "TECH", "UNDEFINED"]
        job_offers.jobProfessions = [{"profession_id": "1", "profession_name": "TEST", "category_name": "TECH"},
                                     {"profession_id": "2", "profession_name": "DEV", "category_name": "TECH"},
                                     {"profession_id": "UNDEFINED", "profession_name": "UNDEFINED", "category_name": "UNDEFINED"}]
        job_offers.jobOpportunities = [{"profession_id": "UNDEFINED", "contract_type": "UNDEFINED", "name": "UNDEFINED",
                                        "office_latitude": "UNDEFINED", "office_longitude": "UNDEFINED"},
                                       {"profession_id": "2", "contract_type": "INTERNSHIP", "name": "QA LEAD",
                                        "office_latitude": "48.885247", "office_longitude": "2.3566441"},
                                       {"profession_id": "2", "contract_type": "CDI", "name": "DEV",
                                        "office_latitude": "48.885247", "office_longitude": "2.3566441"}]
        stats = job_offers.groupByContractAndCategory()
        categories = ["NONE", "TECH", "TOTAL", "UNDEFINED"]
        self.assertEqual([0, 2, 3, 1], [stats["TOTAL"][category] for category in categories])
        self.assertEqual([0, 0, 1, 1], [stats["UNDEFINED"][category] for category in categories])
        self.assertEqual([0, 1, 1, 0], [stats["INTERNSHIP"][category] for category in categories])
        self.assertEqual([0, 1, 1, 0], [stats["CDI"][category] for category in categories])


jobOffers = JobOffers(professions_file="data/technical-test-professions.csv",
                      opportunities_file="data/technical-test-jobs.csv")  # Load data in a JobOffers object
result = jobOffers.groupByContractAndCategory()  # Count offers per contract and per category
if not os.path.exists('output'):
    os.makedirs('output')
with open('output/result.csv', 'w', newline='') as csvfile:  # Export result as a csv file int the "output" directory
    csv_writer = csv.writer(csvfile)
    jobCategories = sorted(list(result.values())[0].keys())
    csv_writer.writerow([""] + [category for category in jobCategories])
    for contractType in sorted(list(result.keys())):
        csv_writer.writerow([contractType] + [result[contractType][category] for category in jobCategories])
